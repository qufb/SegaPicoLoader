/* ###
 * IP: GHIDRA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package segapicoloader;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import ghidra.app.util.Option;
import ghidra.app.util.bin.BinaryReader;
import ghidra.app.util.bin.ByteProvider;
import ghidra.app.util.importer.MessageLog;
import ghidra.app.util.opinion.AbstractLibrarySupportLoader;
import ghidra.app.util.opinion.LoadSpec;
import ghidra.framework.model.DomainObject;
import ghidra.program.flatapi.FlatProgramAPI;
import ghidra.program.model.address.Address;
import ghidra.program.model.data.ByteDataType;
import ghidra.program.model.data.DWordDataType;
import ghidra.program.model.data.DataType;
import ghidra.program.model.data.DataUtilities;
import ghidra.program.model.data.DataUtilities.ClearDataMode;
import ghidra.program.model.data.WordDataType;
import ghidra.program.model.lang.LanguageCompilerSpecPair;
import ghidra.program.model.listing.Program;
import ghidra.program.model.mem.Memory;
import ghidra.program.model.mem.MemoryBlock;
import ghidra.program.model.symbol.SourceType;
import ghidra.program.model.util.CodeUnitInsertionException;
import ghidra.util.exception.CancelledException;
import ghidra.util.task.TaskMonitor;

/**
 * TODO: Provide class-level documentation that describes what this loader does.
 */
public class SegaPicoLoader extends AbstractLibrarySupportLoader {

	private VectorsTable vectors;
	private GameHeader header;

	@Override
	public String getName() {
		return "Sega Pico Loader";
	}

	@Override
	public Collection<LoadSpec> findSupportedLoadSpecs(ByteProvider provider) throws IOException {
		List<LoadSpec> loadSpecs = new ArrayList<>();

		BinaryReader reader = new BinaryReader(provider, false);

		Set<String> names = new HashSet<>(Arrays.asList(
			"IMA IKUNOJYUKU",
			"IMA IKUNOUJYUKU",
			"SAMSUNG PICO",
			"SEGA IAC",
			"SEGA PICO",
			"SEGA TOYS PICO",
			"SEGATOYS PICO"
		));
		if (names.contains(reader.readAsciiString(0x100, 16).strip()) ||
				// Copera games have no product info, but a reset handler at 0x400, so we
				// just match the "SEGA" chars written to 0x800019...
				reader.readAsciiString(0x40e, 4).strip().equals("SEGA")) {
			loadSpecs.add(new LoadSpec(this, 0, new LanguageCompilerSpecPair("68000:BE:32:MC68020", "default"), true));
		}

		return loadSpecs;
	}

	@Override
	protected void load(ByteProvider provider,
			LoadSpec loadSpec,
			List<Option> options,
			Program program,
			TaskMonitor monitor,
			MessageLog log) throws CancelledException, IOException {
		InputStream romStream = provider.getInputStream(0);
		BinaryReader reader = new BinaryReader(provider, false);
		FlatProgramAPI fpa = new FlatProgramAPI(program, monitor);

		header = new GameHeader(reader);
		vectors = new VectorsTable(fpa, reader);

		createSegment(fpa, romStream, "ROM", 0x000000L, Math.min(romStream.available(), 0x3FFFFFL), true, false, true, false, log);
		
		createSegment(fpa, null, "PS/2", 0x200000L, 0x2L, true, true, false, true, log);
		
		createSegment(fpa, null, "IO", 0x800000L, 0x20L, true, true, false, true, log);
		createNamedData(fpa, program, 0x800001L, "IO_VERSION", ByteDataType.dataType, log);
		createNamedData(fpa, program, 0x800003L, "IO_BUTTONS", ByteDataType.dataType, log);
		createNamedData(fpa, program, 0x800005L, "IO_MSB_PEN_X", ByteDataType.dataType, log);
		createNamedData(fpa, program, 0x800007L, "IO_LSB_PEN_X", ByteDataType.dataType, log);
		createNamedData(fpa, program, 0x800009L, "IO_MSB_PEN_Y", ByteDataType.dataType, log);
		createNamedData(fpa, program, 0x80000BL, "IO_LSB_PEN_Y", ByteDataType.dataType, log);
		createNamedData(fpa, program, 0x80000DL, "IO_PAGE", ByteDataType.dataType, log);
		createNamedData(fpa, program, 0x800010L, "IO_PCM_DATA", WordDataType.dataType, log);
		createNamedData(fpa, program, 0x800012L, "IO_PCM_CTRL", WordDataType.dataType, log);
		createNamedData(fpa, program, 0x800019L, "IO_CHAR_S", ByteDataType.dataType, log);
		createNamedData(fpa, program, 0x80001BL, "IO_CHAR_E", ByteDataType.dataType, log);
		createNamedData(fpa, program, 0x80001DL, "IO_CHAR_G", ByteDataType.dataType, log);
		createNamedData(fpa, program, 0x80001FL, "IO_CHAR_A", ByteDataType.dataType, log);

		createSegment(fpa, null, "VDP", 0xC00000L, 0x20L, true, true, false, true, log);
		createNamedData(fpa, program, 0xC00000L, "VDP_DATA", WordDataType.dataType, log);
		createNamedData(fpa, program, 0xC00002L, "VDP__DATA", WordDataType.dataType, log);
		createNamedData(fpa, program, 0xC00004L, "VDP_CTRL", WordDataType.dataType, log);
		createNamedData(fpa, program, 0xC00006L, "VDP__CTRL", WordDataType.dataType, log);
		createNamedData(fpa, program, 0xC00008L, "VDP_CNTR", WordDataType.dataType, log);
		createNamedData(fpa, program, 0xC0000AL, "VDP__CNTR", WordDataType.dataType, log);
		createNamedData(fpa, program, 0xC0000CL, "VDP___CNTR", WordDataType.dataType, log);
		createNamedData(fpa, program, 0xC0000EL, "VDP____CNTR", WordDataType.dataType, log);
		createNamedData(fpa, program, 0xC00011L, "VDP_PSG", ByteDataType.dataType, log);

		createSegment(fpa, null, "RAM", 0xFF0000L, 0x10000L, true, true, true, false, log);
		createMirrorSegment(program.getMemory(), fpa, "RAM", 0xFF0000L, 0xFFFF0000L, 0x10000L, log);

		markHeader(program, fpa, log);
		markVectorsTable(program, fpa, log);

		monitor.setMessage(String.format("%s : Loading done", getName()));
	}

	private void createSegment(FlatProgramAPI fpa,
			InputStream stream,
			String name,
			long address,
			long size,
			boolean read,
			boolean write,
			boolean execute,
			boolean volatil,
			MessageLog log) {
		MemoryBlock block;
		try {
			block = fpa.createMemoryBlock(name, fpa.toAddr(address), stream, size, false);
			block.setRead(read);
			block.setWrite(write);
			block.setExecute(execute);
			block.setVolatile(volatil);
		} catch (Exception e) {
			log.appendException(e);
		}
	}

	private void createNamedData(FlatProgramAPI fpa,
			Program program,
			long address,
			String name,
			DataType type,
			MessageLog log) {
		try {
			if (type.equals(ByteDataType.dataType)) {
				fpa.createByte(fpa.toAddr(address));
			} else if (type.equals(WordDataType.dataType)) {
				fpa.createWord(fpa.toAddr(address));
			} else if (type.equals(DWordDataType.dataType)) {
				fpa.createDWord(fpa.toAddr(address));
			}
			program.getSymbolTable().createLabel(fpa.toAddr(address), name, SourceType.IMPORTED);
		} catch (Exception e) {
			log.appendException(e);
		}
	}

	private void createMirrorSegment(Memory memory,
			FlatProgramAPI fpa,
			String name,
			long base,
			long new_addr,
			long size,
			MessageLog log) {
		MemoryBlock block;
		Address baseAddress = fpa.toAddr(base);
		try {
			block = memory.createByteMappedBlock(name, fpa.toAddr(new_addr), baseAddress, size, false);

			MemoryBlock baseBlock = memory.getBlock(baseAddress);
			block.setRead(baseBlock.isRead());
			block.setWrite(baseBlock.isWrite());
			block.setExecute(baseBlock.isExecute());
			block.setVolatile(baseBlock.isVolatile());
		} catch (Exception e) {
			log.appendException(e);
		}
	}

	private void markHeader(Program program, FlatProgramAPI fpa, MessageLog log) {
		try {
			DataUtilities.createData(program, fpa.toAddr(0x100), header.toDataType(), -1, false,
					ClearDataMode.CLEAR_ALL_UNDEFINED_CONFLICT_DATA);
		} catch (CodeUnitInsertionException e) {
			log.appendException(e);
		}
	}

	private void markVectorsTable(Program program, FlatProgramAPI fpa, MessageLog log) {
		try {
			DataUtilities.createData(program, fpa.toAddr(0), vectors.toDataType(), -1, false,
					ClearDataMode.CLEAR_ALL_UNDEFINED_CONFLICT_DATA);

			for (VectorFunc func : vectors.getVectors()) {
				fpa.createFunction(func.getAddress(), func.getName());
			}
		} catch (CodeUnitInsertionException e) {
			log.appendException(e);
		}
	}
}
