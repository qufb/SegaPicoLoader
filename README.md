# Sega Pico Loader

Sega Pico ROMs loader for GHIDRA.

## References

- [picodoc](https://notaz.gp2x.de/docs/picodoc.txt)
- [GitHub \- lab313ru/ghidra\_sega\_ldr: Sega Mega Drive / Genesis ROMs loader for GHIDRA](https://github.com/lab313ru/ghidra_sega_ldr)
